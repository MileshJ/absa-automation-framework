Welcome to my automated framework for API and Web Testing
**********************************************************************************************************************************************
I have built this framework from scratch over Maven and TestNG
Using the latest version of TestNG you can now conventiently run all tests from the new ui run options over your test annotations
or at the top level annotation to run all tests
I have also built this to run completely out the box ,you can run the following files to see the requsted automation tasks
Web Automation
*RestAssuredAutomatedAPITesting\src\test\java\utils\WebAutomation.java
API Automation
*RestAssuredAutomatedAPITesting\src\test\java\APIAutomation.java
**********************************************************************************************************************************************
I have also used Apache POI and --ooxml to dynamically read test data from a data sheet and used TestNG testAnnotations to allow
for the use of multiple excel files and various sheet combinations
Reguarding Web Automation I have allowed for the second user automation steps to be sent from the webdriver to satisfy two data usages
instead of sending both sets of data on the excel document.
**********************************************************************************************************************************************
Reports are automaticaly generated at:
RestAssuredAutomatedAPITesting\test-output\emailable-report.html
I have added both reports in the folder below for your convenience:
RestAssuredAutomatedAPITesting\Reports
**********************************************************************************************************************************************