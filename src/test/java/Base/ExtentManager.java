package Base;

//import java.io.File;
//import java.util.Date;
//
//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//import com.aventstack.extentreports.reporter.configuration.Theme;
//
//public class ExtentManager {
//	private static ExtentReports extent;
//	
//	public static ExtentReports createInstance()
//	{
//		//extent report configuration
//		String fileName = getReportName();
//		//dynamically setting directory
//		String directory = System.getProperty("user.dir")+"/reports/";
//		new File(directory).mkdirs();
//		String path = directory + fileName;
//		ExtentHtmlReporter htmlreporter=new ExtentHtmlReporter(path);
//		htmlreporter = new ExtentHtmlReporter("./reports/extent.html");
//		htmlreporter.config().setEncoding("utf-8");
//		htmlreporter.config().setDocumentTitle("Automation Reports");
//		htmlreporter.config().setReportName("Automation Test Results");
//		htmlreporter.config().setTheme(Theme.STANDARD);
//		
//		extent = new ExtentReports();
//		extent.setSystemInfo("ABSA Automation Project", "API and Web Automation reports");
//		extent.setSystemInfo("Browser","Chrome");
//		extent.attachReporter(htmlreporter);
//		
//		return extent;
//		
//		
//	}
//	
//
//	public static String getReportName()
//	{
//		Date d= new Date();
//		String fileName= "AutomationReport_" + d.toString().replace(":","_").replace(" ", "_");
//		return fileName;
//	}
//}
