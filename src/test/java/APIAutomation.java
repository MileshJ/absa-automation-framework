

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class APIAutomation {
 
	@Test
	void getDogBreeds()
	{
		//Specifying the END Point
		RestAssured.baseURI="https://dog.ceo/api/breeds";
		
		//The Request Object
		RequestSpecification httprequest=RestAssured.given();
		
		//The Response Object
		Response response =httprequest.request(Method.GET,"/list");
		
		//print response
		String responseBody=response.getBody().asString();
		System.out.println("Response Body is: "+responseBody);
		

	}
	@Test
	void getRandomGoldenRetriverImage()
	{
		//Specifying the END Point
				RestAssured.baseURI="https://dog.ceo/api/breed/retriever/golden/images";
				
				//The Request Object
				RequestSpecification httprequest=RestAssured.given();
				
				//The Response Object
				Response response =httprequest.request(Method.GET,"/random");
				
				//print response
				String responseBody=response.getBody().asString();
				System.out.println("Response Body is: "+responseBody);
		
	}
	
	
	@Test
	void VerifyRetriever()
	{
		//Specifying the END Point
				RestAssured.baseURI="https://dog.ceo/api/breeds";
				
				//The Request Object
				RequestSpecification httprequest=RestAssured.given();
				
				//The Response Object
				Response response =httprequest.request(Method.GET,"/list");
				
				//print response
				String responseBody=response.getBody().asString();
				Assert.assertEquals(responseBody.contains("retriever"), true);
		
	}
	@Test
	void GoldenRetriverSubBreeds()
	{
		//Specifying the END Point
		RestAssured.baseURI="https://dog.ceo/api/breed/retriever";
		
		//The Request Object
		RequestSpecification httprequest=RestAssured.given();
		
		//The Response Object
		Response response =httprequest.request(Method.GET,"/list");
		
		//print response
		String responseBody=response.getBody().asString();
		System.out.println("Response Body is: "+responseBody);	
	}
}
