package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WebAutomation {
	//sending the first set of data from an excel file
	@Test(dataProvider="adminData")
	  public void WebAutomationTest (String firstName,String Username,String lastName,String Password,String Email,String Cell) {
		  String projectPath=System.getProperty("user.dir");
		  System.setProperty("webdriver.chrome.driver",projectPath+"/chromedriver.exe");
		  WebDriver driver= new ChromeDriver();
		  driver.get("http://www.way2automation.com/angularjs-protractor/webtables/");
		  
		  
		  //validation of table
		  driver.findElement(By.xpath("//table[@class='smart-table table table-striped']")).isDisplayed();
		  //click to add a user
		  driver.findElement(By.xpath("//button[@class='btn btn-link pull-right']")).click();
		  
		  //enter first name
		  driver.findElement(By.xpath("//input[@name='FirstName']")).sendKeys(firstName);
		  //enter last name
		  driver.findElement(By.xpath("//input[@name='LastName']")).sendKeys(lastName);
		  //enter username
		  driver.findElement(By.xpath("//input[@name='UserName']")).sendKeys(Username);
		  //enter password
		  driver.findElement(By.xpath("//input[@name='Password']")).sendKeys(Password);
		  //select company AAA
		  driver.findElement(By.xpath("//input[@value='15']")).click();
		  //select Role Drop Down
		  driver.findElement(By.xpath("//select[@name='RoleId']")).click();
		  //select Admin Role
		  driver.findElement(By.xpath("//option[@value='2']")).click();
		  //enter email
		  driver.findElement(By.xpath("//input[@name='Email']")).sendKeys(Email);
		  //enter mobile number
		  driver.findElement(By.xpath("//input[@name='Mobilephone']")).sendKeys(Cell);
		  //clicks save button
		  driver.findElement(By.xpath("//button[@ng-click='save(user)']")).click();
		  try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		//hardcoded these values to supply two varients of passing data as requested
		  //click to add a user
		  driver.findElement(By.xpath("//button[@class='btn btn-link pull-right']")).click();
		  
		  //clearing cached fields
		  driver.findElement(By.xpath("//input[@name='FirstName']")).clear();
		  //enter first name
		  driver.findElement(By.xpath("//input[@name='FirstName']")).sendKeys("FName2");
		  
		  //clearing cached fields
		  driver.findElement(By.xpath("//input[@name='LastName']")).clear();
		  //enter last name
		  driver.findElement(By.xpath("//input[@name='LastName']")).sendKeys("LName2");
		  
		  //clearing cached fields
		  driver.findElement(By.xpath("//input[@name='UserName']")).clear();
		  //enter username
		  driver.findElement(By.xpath("//input[@name='UserName']")).sendKeys("User2");
		  
		  //clearing cached fields
		  driver.findElement(By.xpath("//input[@name='Password']")).clear();
		  //enter password
		  driver.findElement(By.xpath("//input[@name='Password']")).sendKeys("Pass2");
		  //select company BBB
		  driver.findElement(By.xpath("//input[@value='16']")).click();
		  //select Role Drop Down
		  driver.findElement(By.xpath("//select[@name='RoleId']")).click();
		  //select Customer
		  driver.findElement(By.xpath("//option[@value='1']")).click();
		  
		  //clearing cached fields
		  driver.findElement(By.xpath("//input[@name='Email']")).clear();
		  //enter email
		  driver.findElement(By.xpath("//input[@name='Email']")).sendKeys("cusomter@mail.com");
		  
		  //clearing cached fields
		  driver.findElement(By.xpath("//input[@name='Mobilephone']")).clear();
		  //enter mobile number
		  driver.findElement(By.xpath("//input[@name='Mobilephone']")).sendKeys("083444");
		  //clicks save button
		  driver.findElement(By.xpath("//button[@ng-click='save(user)']")).click();
		  
		  driver.close();
		  driver.quit();
	  }
	
	
	@DataProvider(name="adminData")
	public Object[][] getData()
	{ 
		//dynamically pulling any users project path and then utilizing just the data file respective to the project folder
		String projectPath=System.getProperty("user.dir");
		String excelPath=projectPath+"/Excel/data.xlsx";
		Object data[][]= testData(excelPath, "Sheet1");
		return data;
	} 
	
	   
	
	public static Object[][] testData(String excelPath,String sheetName)
{
	ExcelUtils excel= new ExcelUtils(excelPath, sheetName);
	int rowCount=excel.getRowCount();
	int colCount=excel.getColCount();
	//using object for a variety of data types
	Object data[][] = new Object[rowCount-1][colCount];
	//start at one because of headers,count for as many rows exist
	for(int i=1;i<rowCount;i++)
	{
		for(int j=0;j<colCount;j++)
		{
			String cellData=excel.getCellDataString(i,j);
			System.out.print(cellData+ " | ");
			data[i-1][j]=cellData;
		}
		System.out.println();
	}
	return data;
}
	
	


}
