package utils;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ExcelUtils {
	
	//global variables for easy reusability
	static String projectPath;
	static XSSFWorkbook workbook;
	static XSSFSheet sheet;
	
	//using this to reconstruct everytime a new keyword is called
	public ExcelUtils(String excelPath, String sheetName)
	{
	 
		
		try {
			//dynamically call the file from the users directory so anyone can run
			workbook = new XSSFWorkbook(excelPath);
			sheet = workbook.getSheet(sheetName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	public static int getRowCount()
	{
		int rowCount=0;
		
		
		try {
			
			//retrieve the amount of rows in the file
			rowCount=sheet.getPhysicalNumberOfRows();
			System.out.println("No of rows : "+rowCount);
		} catch (Exception e) {
			//incase an exception occures this will handle the output\
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		//assigning where to look in the workbook for data
		
		return rowCount;
	}
	
	public static String getCellDataString(int rowNum,int colNum) {
		//dynamically call the file from the users directory so anyone can run 
		String cellData=null;
		try {
			//get row and cell data to allow us to ensure we pull the correct data from the datasheet
			cellData=sheet.getRow(rowNum).getCell(colNum).getStringCellValue();
			System.out.println(cellData);
		} catch (Exception e) {
			//incase an exception occures this will handle the output\
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
		return cellData;
	}
	
	public static void getCellDataNumber(int rowNum,int colNum) {
		//dynamically call the file from the users directory so anyone can run 
		
		try {
			//get row and cell data to allow us to ensure we pull the correct data from the datasheet
			double cellData=sheet.getRow(rowNum).getCell(colNum).getNumericCellValue();
		} catch (Exception e) {
			//incase an exception occures this will handle the output\
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
	
	}
	
	public static int getColCount() {
		//dynamically call the file from the users directory so anyone can run 
	
		int colCount=0;
		try {
			//get row and cell data to allow us to ensure we pull the correct data from the datasheet
			colCount=sheet.getRow(0).getPhysicalNumberOfCells();
			System.out.println("No of coloumns : " +colCount);
			//double cellData=sheet.getRow(rowNum).getCell(colNum).getNumericCellValue();
		} catch (Exception e) {
			//incase an exception occures this will handle the output\
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
	return colCount;
	}
}
