package testclasses;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class TestClassUsingListeners {
public WebDriver driver;


@BeforeClass
public void beforeClass()
{
//	driver = new ChromeDriver();
//	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
//	driver.get("http://www.way2automation.com/angularjs-protractor/webtables/");
}

@AfterClass
public void afterClass()
{
	driver.quit();
}

@Test
public void testSuccessful()
{
	System.out.println("Executing successful tests");
	
}
@Test
public void testFailed()
{
	System.out.println("Executing failed tests");
	Assert.fail("Executing failed test method");
	
}
@Test
public void testSkipped()
{
	System.out.println("Executing skipped tests");
	
}


}
